using HW1.Data;
using HW1.Enums;
namespace HW1.Data
{
    public class Crew
    {
        public int Id { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public string Name { get; set; }
    }
}