using System.Collections.Generic;
using System.Linq;
using HW1.Data;
using HW1.Enums;
namespace HW1.Data
{
    public class Flight
    {
        public string Id { get; set; }
        public List<Crew> Crew { get; set; }
        public Crew PositivePilot => Crew.FirstOrDefault(x => x.EmployeeType == EmployeeType.PositivePilot);
        public Crew DeputyPilot => Crew.FirstOrDefault(x => x.EmployeeType == EmployeeType.DeputyPilot);
    }
}