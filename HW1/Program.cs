﻿using System;
using System.Collections.Generic;
using System.Linq;
using HW1.Data;
using HW1.Enums;

namespace HW1
{
    class Program
    {
        static void Main(string[] args)
        {
            var crews = new List<Crew>() {
                new Crew () { Id = 1, EmployeeType = EmployeeType.PositivePilot, Name = "Hotch" },
                new Crew () { Id = 2, EmployeeType = EmployeeType.DeputyPilot, Name = "Jack" },
                new Crew () { Id = 3, EmployeeType = EmployeeType.CA, Name = "Nick" },
                new Crew () { Id = 4, EmployeeType = EmployeeType.CA, Name = "Mark" },
                new Crew () { Id = 5, EmployeeType = EmployeeType.CA, Name = "Joz" }
            };

            var flight = new Flight()
            {
                Id = "BR3030",
                Crew = crews
            };

            string output = string.Format($"{flight.Id} 機師是{ flight.PositivePilot.Name}和{flight.DeputyPilot.Name}，共有CA人數{flight.Crew.Where(x => x.EmployeeType == EmployeeType.CA).ToList().Count}人");

            Console.WriteLine(output);
        }
    }
}