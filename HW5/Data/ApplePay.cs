using System;
namespace HW5.Data
{
    public class ApplePay : ICashier
    {
        public void Payment(int amount)
        {
            Console.WriteLine($"已使用ApplePay（結帳方式）收款{amount}元");
        }

    }
}