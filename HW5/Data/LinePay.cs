using System;
namespace HW5.Data
{
    public class LinePay : ICashier
    {
        public void Payment(int amount)
        {
            Console.WriteLine($"已使用Linepay（結帳方式）收款{amount}元");
        }
    }
}