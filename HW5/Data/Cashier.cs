using System;
namespace HW5.Data
{
    public class Cashier
    {
        public ICashier CashierPayment(string payment)
        {

            switch (payment)
            {
                case "ApplePay":
                    return new ApplePay();
                case "LinePay":
                    return new LinePay();
                case "Cash":
                    return new Cash();
                case "CreditCard":
                    return new CreditCard();
                default:
                    return null;
            }

        }
    }
}