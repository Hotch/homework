using System;
namespace HW5.Data
{
    public class Cash : ICashier
    {
        public void Payment(int amount)
        {
            Console.WriteLine($"已使用Cash（結帳方式）收款{amount}元");
        }
    }
}