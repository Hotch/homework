using System;
namespace HW5.Data
{
    public class CreditCard : ICashier
    {
        public void Payment(int amount)
        {
            Console.WriteLine($"已使用CreditCard（結帳方式）收款{amount}元");
        }
    }
}