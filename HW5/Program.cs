﻿using System;
using HW5.Data;
namespace HW5
{
    class Program
    {
        static void Main(string[] args)
        {
            var cashier = new Cashier();
            var applePay = cashier.CashierPayment("ApplePay");
            applePay.Payment(5566);
            var cash = cashier.CashierPayment("Cash");
            cash.Payment(556);
            var creditCard = cashier.CashierPayment("CreditCard");
            creditCard.Payment(123);
            var linePay = cashier.CashierPayment("LinePay");
            linePay.Payment(56);
        }
    }
}