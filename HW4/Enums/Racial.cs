namespace HW4.Enums
{
    public enum Racial
    {
        Saiyan,
        SuperSaiyan,
        Meikexing,
        Earthling
    }
}