using System;
using HW4.Data;
using HW4.Enums;
namespace HW4
{
    public static class Func
    {


        public static int GetDefaultAttack(Racial racial)
        {
            Random random = new Random();
            switch (racial)
            {
                case Racial.Saiyan:
                    return random.Next(70, 90);
                case Racial.Meikexing:
                    return random.Next(80, 120);
                case Racial.Earthling:
                    return random.Next(50, 100);
                default:
                    return -1;
            }
        }

        public static void Fight(Human human1, Human human2)
        {
            Console.WriteLine($"{human1.Name}({human1.Racial}) HP:{human1.HP} Attack:{human1.Attack}");
            Console.WriteLine($"{human2.Name}({human2.Racial}) HP:{human2.HP} Attack:{human2.Attack}");

            while (human1.IsAlive && human2.IsAlive)
            {
                Console.WriteLine($"{human1.Name}({human1.Racial})(HP:{human1.HP})(Attack:{human1.Attack}) 發動攻擊 {human2.Name}({human2.Racial})(HP{human2.HP})(Attack:{human2.Attack})");
                if (human1.Attack < human2.Attack)
                {
                    Console.WriteLine($"{human1.Name}({human1.Racial})(HP:{human1.HP}) 被反擊 -HP{(human2.Attack - human1.Attack)}");

                    human1.HP -= (human2.Attack - human1.Attack);
                }
                else
                {
                    human2.HP -= human1.Attack;
                }

                Console.WriteLine($"{human2.Name}({human2.Racial})(HP:{human2.HP})(Attack:{human2.Attack}) 發動攻擊 {human1.Name}({human1.Racial})(HP:{human1.HP})(Attack:{human1.Attack})");

                human1.HP = human1.HP - human2.Attack;
            };

        }


        public static int GetDefaultAttack(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

    }
}