﻿using System;
using HW4.Data;
using HW4.Enums;
namespace HW4
{
    class Program
    {
        static void Main(string[] args)
        {

            // 第一局 地球人對那美克星人
            // 第二局 地球人對賽亞人
            // 第三局 賽亞人對那美克星人
            for (int i = 0; i < 3; i++)
            {
                switch (i)
                {
                    case 0:
                        Human earthling = new Human("Hotch", Racial.Earthling);
                        Human meikexing = new Human("Choco", Racial.Meikexing);
                        Console.WriteLine("------------第一局 地球人對那美克星人---------------");
                        Func.Fight(earthling, meikexing);
                        Console.WriteLine("------------戰鬥結束---------------");
                        break;
                    case 1:
                        Human earthling2 = new Human("Hotch", Racial.Earthling);
                        Human saiyan = new Human("Choco", Racial.Saiyan);
                        Console.WriteLine("------------第二局 地球人對賽亞人---------------");
                        Func.Fight(earthling2, saiyan);
                        Console.WriteLine("------------戰鬥結束---------------");
                        break;
                    case 2:
                        Human saiyan2 = new Human("Hotch", Racial.Saiyan);
                        Human meikexing2 = new Human("Choco", Racial.Meikexing);
                        Console.WriteLine("------------第三局 賽亞人對那美克星人---------------");
                        Func.Fight(saiyan2, meikexing2);
                        Console.WriteLine("------------戰鬥結束---------------");
                        break;

                }
            }

        }
    }
}