using System;
using HW4.Enums;
namespace HW4.Data
{
    public class Human
    {
        private int _attack;
        private int _defaultAttack;
        private int _HP;
        private int _defaultHP = 1000;
        private int _canUseSupperAttackCount;

        public Human(string name, Racial racial)
        {
            _defaultAttack = Func.GetDefaultAttack(racial);
            Name = name;
            Racial = racial;
            Attack = _attack = _defaultAttack;
            HP = _HP = _defaultHP;
        }
        public string Name { get; set; }
        public int Attack
        {
            get
            {
                return _attack;
            }
            set
            {
                _attack = value;
            }
        }
        public int HP
        {
            get
            {
                return _HP;
            }
            set
            {

                _HP = (value <= 0) ? 0 : value;

                if (_HP != _defaultHP)
                {
                    Console.WriteLine($"{Name}({Racial})的HP{_HP}");
                }

                if (_HP <= 0)
                {
                    IsAlive = false;
                    Console.WriteLine($"{Name} 死了！");
                }

                if ((double)_HP / _defaultHP * 100.0 < 30.0 && CanHealing && Racial == Racial.Meikexing && IsAlive)
                {
                    Console.WriteLine($"{Name} HP{HP} 低於30% 回覆生命到80%！");
                    _HP = (int)(_defaultHP * 0.8);
                    CanHealing = false;
                }

                if (Racial == Racial.SuperSaiyan)
                {
                    _canUseSupperAttackCount -= 1;
                }

                if ((double)_HP / _defaultHP * 100.0 < 30.0 && Racial == Racial.Saiyan && IsAlive)
                {
                    Racial = Racial.SuperSaiyan;
                    Attack = _attack * 3;
                    _canUseSupperAttackCount = 2;

                    Console.WriteLine($"{Name}的HP{_HP} 低於30% 變成超級賽亞人了！ 攻擊力提升{Attack} 超級攻擊次數{_canUseSupperAttackCount}");
                }

                if (_canUseSupperAttackCount == 0 && Racial == Racial.SuperSaiyan && IsAlive)
                {
                    Racial = Racial.Saiyan;
                    Attack = _defaultAttack;
                    Console.WriteLine($"{Name} 變回賽亞人了！");
                }
            }
        }
        public Racial Racial { get; set; }
        public bool IsAlive { get; set; } = true;
        public bool CanHealing { get; set; } = true;
        public int CanUseSupperAttackCount
        {
            get
            {
                return _canUseSupperAttackCount;
            }
            set
            {
                _canUseSupperAttackCount = value;
            }
        }
    }
}