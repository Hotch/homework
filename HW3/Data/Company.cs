using System.Collections.Generic;

namespace HW3.Data
{
    public class Company
    {
        public Company(string companyName)
        {
            CompanyName = companyName;
            Employees = new List<Employee>();
        }
        public string CompanyName { get; set; }
        public List<Employee> Employees {get;set;}
    }
}