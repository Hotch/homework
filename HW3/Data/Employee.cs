using HW3.Enums;
namespace HW3.Data
{
    public class Employee
    {

        public Employee(string name, Career career, string company)
        {
            Name = name;
            Career = career;
            Company = company;

        }
        public string Name { get; set; }
        public Career Career { get; set; }
        public string Company { get; set; }

        public string Introduction()
        {

            return $"我是{Name}，我的職業是{Career.ToString()}，就職於{Company}";
        }
    }
}