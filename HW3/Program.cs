﻿using System;
using HW3.Enums;
using HW3.Data;
using System.Linq;

namespace HW3
{
    class Program
    {
        static void Main(string[] args)
        {
            var company = new Company("EVA");

            company.Employees.Add(new Employee("Hotch", Career.Engineer, company.CompanyName));
            company.Employees.Add(new Employee("Trebor", Career.Teacher, company.CompanyName));
            company.Employees.Add(new Employee("Choco", Career.Doctor, company.CompanyName));
            company.Employees.Add(new Employee("Drew", Career.Chef, company.CompanyName));
            company.Employees.Add(new Employee("FuYa", Career.Chef, company.CompanyName));

            Output(company);

        }

        private static void Output(Company company)
        {
            Console.WriteLine(
            $"{company.CompanyName}公司" +
            $"有{company.Employees.Where(x => x.Career == Career.Engineer).Count()}名工程師，" +
            $"{company.Employees.Where(x => x.Career == Career.Chef).Count()}名廚師，" +
            $"{company.Employees.Where(x => x.Career == Career.Doctor).Count()}名醫師，" +
            $"{ company.Employees.Where(x => x.Career == Career.Teacher).Count()}名老師"
            );

            foreach (var emp in company.Employees)
            {
                Console.WriteLine(emp.Introduction());
            }
        }

    }
}
