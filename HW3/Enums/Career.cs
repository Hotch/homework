using System.ComponentModel;

namespace HW3.Enums
{
    public enum Career
    {
        [Description("工程師")]
        Engineer,
        [Description("廚師")]
        Chef,
        [Description("醫師")]
        Doctor,
        [Description("老師")]
        Teacher
    }
}