using System;
using HW2.Emuns;
namespace HW2.Data
{
    public class Building
    {
        public Building()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
        public BuildingType Type { get; set; }

    }
}