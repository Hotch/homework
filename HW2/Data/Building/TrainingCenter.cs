using HW2.Emuns;
namespace HW2.Data
{
    public class TrainingCenter : Building, IBuilding<Knight>
    {
        public TrainingCenter()
        {
            Type = BuildingType.TrainingCenter;
        }
        public Knight CreateHuman()
        {
            return new Knight();
        }
    }
}