namespace HW2.Data {
    public interface IBuilding<T> {
        T CreateHuman ();
    }
}