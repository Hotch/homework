using HW2.Emuns;
namespace HW2.Data
{
    public class Church : Building, IBuilding<Monk>
    {
        public Church()
        {
            Type = BuildingType.Church;
        }
        public Monk CreateHuman()
        {
            return new Monk();
        }

    }
}