using HW2.Emuns;
namespace HW2.Data
{
    public class Farmhouse : Building, IBuilding<Farmer>
    {
        public Farmhouse()
        {
            Type = BuildingType.Farmhouse;
        }
        public Farmer CreateHuman()
        {
            return new Farmer();
        }

    }
}