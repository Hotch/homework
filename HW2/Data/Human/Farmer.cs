using System;
using HW2.Emuns;
namespace HW2.Data
{
    public class Farmer : Human
    {

        public Farmer()
        {
            Career = Career.Farmer;
        }

        public T CreateBuilding<T>()
        {

            return (T)Activator.CreateInstance(typeof(T));
        }
    }
}