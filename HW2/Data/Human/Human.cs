using System;
using HW2.Emuns;
namespace HW2.Data
{
    public class Human
    {
        public Human()
        {
            Id = Guid.NewGuid();

        }
        public Guid Id { get; set; }
        public Career Career { get; set; }

    }
}