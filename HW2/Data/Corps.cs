using System.Collections.Generic;
using System.Linq;
using HW2.Emuns;
namespace HW2.Data
{
    public class Corps
    {

        private List<Human> _human = new List<Human>();
        private Farmer _defaultFarmer = new Farmer();
        public Corps()
        {
            _human.Add(_defaultFarmer);
            Human = _human;
            Building = new List<Building>();
        }
        public string Id { get; set; }

        public List<Human> Human { get; set; }

        public List<Building> Building { get; set; }

        public Farmer DefaultHuman => _defaultFarmer;

    }
}