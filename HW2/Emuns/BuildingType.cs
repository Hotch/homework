namespace HW2.Emuns
{
    public enum BuildingType
    {
        Church,
        Farmhouse,
        TrainingCenter
    }
}