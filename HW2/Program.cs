﻿using System;
using System.Linq;
using HW2.Emuns;
using HW2.Data;
namespace HW2
{
    class Program
    {
        static void Main(string[] args)
        {
            var corps = new Corps();

            var church = corps.DefaultHuman.CreateBuilding<Church>();
            var farmhouse = corps.DefaultHuman.CreateBuilding<Farmhouse>();
            var trainingCenter = corps.DefaultHuman.CreateBuilding<TrainingCenter>();

            corps.Building.Add(church);
            corps.Building.Add(farmhouse);
            corps.Building.Add(trainingCenter);

            corps.Human.Add(church.CreateHuman());
            corps.Human.Add(church.CreateHuman());
            corps.Human.Add(church.CreateHuman());
            corps.Human.Add(farmhouse.CreateHuman());
            corps.Human.Add(trainingCenter.CreateHuman());

            string output = $"軍團一共有 {corps.Human.Where(x => x.Career == Career.Farmer).ToList().Count}個農民" +
            $"{corps.Human.Where(x => x.Career == Career.Monk).ToList().Count}個僧侶" +
            $"{corps.Human.Where(x => x.Career == Career.Knight).ToList().Count}個騎士" +
            $"{corps.Building.Where(x => x.Type == BuildingType.Farmhouse).ToList().Count}棟農舍" +
            $"{corps.Building.Where(x => x.Type == BuildingType.Church).ToList().Count}棟教堂" +
            $"{corps.Building.Where(x => x.Type == BuildingType.TrainingCenter).ToList().Count}棟訓練所";

            Console.WriteLine(output);
        }
    }
}
